package adapter

import "fmt"

// service
type Mac struct {
}

func (m *Mac) insertIntoLightningPort() {
	fmt.Println("Lightning connector is plugged into mac machine.")
}
