package decorator

// component interface
type Pizza interface {
	GetPrice() int
}
