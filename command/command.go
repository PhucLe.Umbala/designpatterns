package command

// command interface
type command interface {
	execute()
}
