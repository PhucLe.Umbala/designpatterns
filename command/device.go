package command

// receiver interface
type device interface {
	on()
	off()
}
