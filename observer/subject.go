package observer

type Subject interface {
	Register(Observer Observer)
	UnRegister(Observer Observer)
	NotifyAll()
}
