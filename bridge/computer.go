package bridge

// abstraction
type Computer interface {
	Print()
	SetPrinter(Printer)
}
