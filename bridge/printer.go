package bridge

// implementation
type Printer interface {
	PrintFile()
}
